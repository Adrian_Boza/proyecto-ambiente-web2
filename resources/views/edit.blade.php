@extends('layouts.app')
<style>
            html, body {
                background-image: url('/img/client.jpg');
                font-family: 'Numans', sans-serif;
            }

</style>

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card"  style="background-color: rgba(0,0,0,0.5); color: #fff; width:110%; height: 110%;margin-top:7%; ">
                <div class="card-header" style="text-align: center; font-size:22px;">{{ __('Edit Tree') }}</div>

                <div class="card-body"style="margin-right:10%; margin-top:4%;">
                             
                             @foreach ($trees as $tree)      
                            <form action="{{ url('/tree/'. $tree->id) }}" method="post" enctype="multipart/form-data"> 
							   @csrf
							   @method('PATCH')
                               
                                  
                                <input type="number" name="height" id="altura" placeholder=" Altura " onfocus="this.placeholder = ''" onblur="this.placeholder = ' Altura '" required="" value="{{ $tree->height }}" >

                                <input type="text" name="name" id="especie" placeholder=" Nombre Del Árbol " onfocus="this.placeholder = ''" onblur="this.placeholder = ' Nombre Del Árbol '" required=""  value="{{ $tree->name }}" style="margin-left:55%; margin-top: -4.3%">

                            	
                                <button type="submit" id="btnSave" style="margin-top:12%" >Save</button>
                            </form>
                       		 @endforeach  

                       
                    </form>
                </div>
            </div>
        </div>
    </div>



@endsection