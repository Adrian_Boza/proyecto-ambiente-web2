

@extends('index')

@section('conten')

        
         <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel" style="height:600px">
          <ol class="carousel-indicators" >
            <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
          </ol>
          <div class="carousel-inner" >
            <div class="carousel-item active" style="height:610px">
              <img src="img/inicio1.jpg" class="d-block w-100"  style="height:610px" alt="...">
              <div class="carousel-caption d-none d-md-block" >
                <h5 style="margin-top:50px">Sobre Un Millón De Arboles</h5>
                <p>Construir conciencia colectiva, para realizar cambios reales, en la forma convivir en el mundo actual, donde la naturaleza y el "desarrollo humano" impacta en nuestra salud, el cambio climatico, enfocando la importancia de los arboles...</p>
              </div>
            </div>
            <div class="carousel-item" style="height:610px">
              <img src="img/about2.jpg" class="d-block w-100"  style="height:610px" alt="...">
              <div class="carousel-caption d-none d-md-block">
                <h5>Nuestro proposito</h5>
                <p>Somos una campaña preocupados por ayudar al medio ambiente mediante diversas actividades dentro del ámbito de la conservación del medio ambiente.</p>
              </div>
            </div>
            <div class="carousel-item" style="height:610px">
              <img src="img/inicio.jpg" class="d-block w-100"  style="height:610px" alt="...">
              <div class="carousel-caption d-none d-md-block">
                <h5>Sobre la página</h5>
                <p>Es una pagina donde puedes encontrar contenido sobre todo en torno al cuidado del medio ambiente asi como consejos o dudas en torno biologia especialmente plantas asi como su facilitacion de estas</p>
              </div>
            </div>
          </div>
          <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      
     

    @endsection 
