@extends('layouts.app')
<style>
            html, body {
                background-image: url('/img/client.jpg');
                font-family: 'Numans', sans-serif;
            }

</style>
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8"style="margin-left: -28%">
            <div class="card" style="background-color: rgba(0,0,0,0.5); color: #fff; width:140%;    height: 500px;margin-top:2%;">
                <div class="card-header" style="text-align: center; font-size:22px;">Dashboard</div>

                <div class="card-body" >
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if(Auth::user()->admin)
                     <ul class="nav nav-tabs" id="myTab" role="tablist" >
                      <li class="nav-item" >
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Dashboard</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Árboles </a>
                      </li>
                      
                     </ul>
                     <div class="tab-content" id="myTabContent">
                      <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="input-group form-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-user"></i></span>
                                </div>
                                 @foreach ($cantUsers as $user)
                                   <p class="name" >Cantidad de amigos registrados: {{$user->cantu}}</p>
                                @endforeach
                        </div>

                        <div class="input-group form-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-tree"></i></span>
                                </div>
                                @foreach ($cantTrees as $tree)
                                   <p class="name" > Cantidad de árboles sembrados: {{$tree->cantt}}</p>
                                @endforeach
                        </div>



                      </div>
                      <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                          
                        <h2>Árboles Amigos</h2> 
                            <div class="table-wrapper-scroll-y my-custom-scrollbar">
                                <table class="table table-bordered table-striped mb-0 text-center" >
                                    <thead style="color: #FFF;">
                                      <th>#</th>
                                      <th>Name</th>
                                      <th>Height</th>
                                      <th>Owner</th>
                                      <th>Species</th>
                                      <th>View Photo</th>
                                      <th>Actions</th>
                                    </thead>
                                    <tbody style="color: #FFF;">
                                      @foreach ($tableUsers as $table)
                                       <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $table->name }}</td>
                                        <td>{{ $table->height }}</td>
                                        <td>{{ $table->fullname }}</td>
                                        <td>{{ $table->speciesname }}</td>
                                        <td>
                                             <a href='{{  url('/tree/'.$table->specie_id) }}' class='btn btn-info' style="background-color: rgba(0,0,0,0.5);color: #fff; border: none;">View Photo</a>
                                        </td>
                                        <td>
                                        <a href='{{  url('/tree/'.$table->id.'/edit') }}' class='btn btn-info' style="background-color: rgba(0,0,0,0.5);color: #fff; border: none;">Edit</a>

                                        </td>
                                       </tr>
                                      @endforeach
                                    </tbody>
                                </table>
                            </div>    
                       </div>
                            
                             

                   
                    </div>



                    @else
                    <ul class="nav nav-tabs" id="myTab" role="tablist" >
                      <li class="nav-item" >
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Dashboard</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Árboles </a>
                      </li>
                      
                    </ul>
                    <div class="tab-content" id="myTabContent">
                      <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

                        <form action="{{ route('tree.store') }}" method="post">
                            @csrf
                            <select name="combo" id="comboEspecies" onchange="" >
                                 <option value="">Selecione una especie de árbol...</option>
                                    @foreach ($species as $specie)
                                        <option value="{{ $specie->id }}">{{ $specie->name }}</option>
                                    @endforeach 
                            </select> 
                            

                            <input type="text" name="nom" id="nom" placeholder=" Nombre " onfocus="this.placeholder = ''" onblur="this.placeholder = 'Nombre '" required="" >
                             

                            <input type="number" id="monto" name="monto" placeholder=" Monto para donar " onfocus="this.placeholder = ''" onblur="this.placeholder = 'Monto para donar  '" required="" > 
                            
                             <button type="submit" id="btnSav">Comprar Árbol</button>
                        </form>




                      </div>
                      <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                          
                        <h2>Mis Árboles</h2> 
                            <div class="table-wrapper-scroll-y my-custom-scrollbar">
                                <table class="table table-bordered table-striped mb-0 text-center" >
                                    <thead style="color: #FFF;">
                                      <th>#</th>
                                      <th>Name</th>
                                      <th>Height</th>
                                      <th>Species</th>
                                      <th>Foto</th>
                                    </thead>
                                    <tbody style="color: #FFF;">
                                      @foreach ($trees as $table)
                                       <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $table->name }}</td>
                                        <td>{{ $table->height }}</td>
                                        <td>{{ $table->speciesname }}</td>
                                         <td>
                                         <a href='{{  url('/tree/'.$table->specie_id) }}' class='btn btn-info' style="background-color: rgba(0,0,0,0.5);color: #fff; border: none;">View Photo</a>
                                        </td>
                                       </tr>
                                      @endforeach
                                    </tbody>
                                </table>
                            </div>    

                         
                       </div>
                            
                             

                      
                    </div>
                    

                        
                      
                @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
