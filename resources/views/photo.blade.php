@extends('layouts.app')
<style>
            html, body {
                background-image: url('/img/client.jpg');
                font-family: 'Numans', sans-serif;
            }

</style>

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card"  style="background-color: rgba(0,0,0,0.5); color: #fff; width:110%; height: 110%;margin-top:7%; ">
                <div class="card-header" style="text-align: center; font-size:22px;">{{ __('Edit Tree') }}</div>

                <div class="card-body"style="margin-right:10%; margin-top:4%;">
                             
                            @foreach ($imgs as $img)      
                                
                                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" >
                                      <div class="carousel-inner" style="height:220px; width:350px;  margin-left: 28%; margin-top:3.5%;">
                                        <div class="carousel-item active" >
                                          <img class="d-block w-100" src="/{{ $img->imgone }}" alt="First slide" style="height:220px; width:450px;">
                                        </div>
                                        <div class="carousel-item">
                                          <img class="d-block w-100" src="/{{ $img->imgtwo }}" alt="Second slide" style="height:220px; width:450px;" >
                                        </div>
                                        <div class="carousel-item">
                                          <img class="d-block w-100" src="/{{ $img->imgthree }}" alt="Third slide" style="height:220px; width:450px;">
                                        </div>
                                      </div>
                                      <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                      </a>
                                      <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                      </a>
                                </div>

                       		@endforeach  

                       
                    </form>
                </div>
            </div>
        </div>
    </div>



@endsection