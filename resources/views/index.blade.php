
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="{{asset('img/icon.png')}}" type="image/png">
        <title>Un Millón De Árboles</title>
        <!-- Bootstrap CSS -->
        
        <link rel="stylesheet" type="text/css" href="{{asset('bootstraps/css/bootstrap.css')}} ">
        <link rel="stylesheet" type="text/css" 
        href="{{asset('https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css')}}" 
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">


        <!--Fontawesome CDN-->
        <link rel="stylesheet" type="text/css"  href="{{asset('https://use.fontawesome.com/releases/v5.3.1/css/all.css')}}" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

        <!-- Fonts -->
        <link type="text/css"  href="{{asset('https://fonts.googleapis.com/css?family=Nunito:200,600')}}" rel="stylesheet">
       
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
     @if (Route::has('login'))
        <nav class="navbar navbar-expand-lg navbar-light bg-light" style="">
        @auth
          <a class="navbar-brand" href="{{ url('/home') }}" >Home</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
        @else  
          <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
              <li class="nav-item active">
                <a class="nav-link" href="{{ route('login') }}">Login <span class="sr-only">(current)</span></a>
              </li>

            @if (Route::has('register'))
               <li class="nav-item active">
                <a class="nav-link" href="{{ route('register') }}">Register <span class="sr-only">(current)</span></a>
              </li>
            @endif
           @endauth 
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Redes Sociales
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="#">Facebook</a>
                  <a class="dropdown-item" href="#">Instagram</a>
                  <a class="dropdown-item" href="#">Twitter</a>
                </div>
              </li>
            </ul>
          </div>
        @endif
            <img class="icon" src="{{asset('img/icon.png')}}" style="height: 80px;width: 80px; margin-right: 100px"/>
        </nav>

        @yield('conten')
        
    </body>
     <!-- jQuery first, then Popper.js, then Bootstrap JS -->
         <script src="{{asset('bootstraps/js/jquery-3.3.1.min.js')}}"></script>
         <script src="{{asset('bootstraps/js/bootstrap.min.js')}}"></script>
        
</html>
