<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tree extends Model
{
    protected $fillable = [
        'name', 'height', 'amount', 'specie_id', 'user_id',
    ];

    public function species(){
    	return $this->belongsTo('App\Species', 'id', 'specie_id');
    }

    public function user(){
    	return $this->belongsTo('App\User');
    }
}
