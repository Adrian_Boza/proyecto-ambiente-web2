<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Species extends Model
{
    protected $fillable = [
        'name','one','two','three',
    ];

    public function trees(){
    	return $this->hasMany('App\Tree');
    }
}
