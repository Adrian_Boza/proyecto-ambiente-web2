<?php

namespace App\Http\Controllers;
use App\Tree;
use App\User;
use App\Species;
use Redirect;
use Auth;
use Illuminate\Http\Request;

class TreeController extends Controller
{


    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {    
         
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
     
        Tree::create([
           
            'name' => $request['nom'],
            'height'=>1,
            'amount'=>$request['monto'],
            'specie_id'=>$request['combo'],
            'user_id'=>Auth::user()->id,
        ]);
        
        return Redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \home
     */
    public function show($id)
    {
       
        $informations = \DB::table('trees')
        ->join('species', 'trees.specie_id', '=', 'species.id')
        ->select('trees.name','trees.height','species.name')
        ->where('species.id', $id)
        ->get();
       
       
       return view('home',compact('informations'));
    }

    /**
    *regarding the id he  will return a user
    *
    * @param int $id
    */
    public function getImg($id)
    {
       
        $imgs = \DB::table('species')
        ->select('one as imgone','two as imgtwo','three as imgthree')
        ->where('id', $id)
        ->get();
       
       
       return view('photo',compact('imgs'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $trees =  \DB::table('trees')
        ->join('species', 'trees.specie_id', '=', 'species.id')
        ->join('users', 'trees.user_id', '=', 'users.id')
        ->select('trees.name','trees.height','trees.id as id')
        ->where('trees.id', $id)
        ->get();
        
        return view('edit',compact('trees'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateTree = request()->except(['_token','_method']);
        Tree::where('id','=',$id)->update($updateTree);
        return Redirect()->route('home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
    }
}
