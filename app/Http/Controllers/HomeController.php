<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use App\Species;
use App\Tree;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
         $tableUsers = \DB::table('trees')
        ->join('species', 'trees.specie_id', '=', 'species.id')
        ->join('users', 'trees.user_id', '=', 'users.id')
        ->select('trees.name',\DB::raw("CONCAT(users.name,' ',users.lastname) as fullname"),'trees.height','species.name as speciesname','trees.id as id','trees.specie_id')
        ->get();

         $trees =  \DB::table('trees')
        ->join('species', 'trees.specie_id', '=', 'species.id')
        ->join('users', 'trees.user_id', '=', 'users.id')
        ->select('trees.name','trees.height','species.name as speciesname','trees.specie_id')
        ->where('trees.user_id', Auth::user()->id)
        ->get();

         $cantUsers = \DB::table('users')->select(\DB::raw('count(*) as cantu'))->where('admin', false)->get();
         $cantTrees = \DB::table('trees')->select(\DB::raw('count(*) as cantt'))->get();
         $species = Species::all();
         return view('home',compact('species','trees','cantUsers','cantTrees','tableUsers'));

    }

   
}
